import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadImovelComponent } from './components/cad-imovel/cad-imovel.component';
import { CadImobiliariaComponent } from './components/cad-imobiliaria/cad-imobiliaria.component';
import { CadPropietarioComponent } from './components/cad-propietario/cad-propietario.component';
import { CadLocadorComponent } from './components/cad-locador/cad-locador.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    CadImovelComponent,
    CadImobiliariaComponent,
    CadPropietarioComponent,
    CadLocadorComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
