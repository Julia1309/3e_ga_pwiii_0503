export class imovel{
   
    _endereco: String; //Rua e número
    _bairro: String;
    _cep: String;
    _cidade: String;
    _uf: String;
    _qtdQuartos: number;
    _qtdSalas: number;
    _qtdBanheiros: number;
    _qtdCozinhas: number;
    _andares: number;
    _complemento: String;
    _valorVenal: number;
    _valorLocacao: number;
    _isLocavel: boolean;
    _isVenal: boolean;
    _situacao: String;

    constructor(endereco: String, bairro: String, cep: String, cidade: String, uf: String, 
      qtdQuartos: Number, qtdSalas: Number, qtdBanheiros: Number, qtdCozinhas: Number,
      andares: Number, complemento: String, valorVenal: number, valorLocacao: Number,
      isLocavel: boolean, isVenal: boolean, situacao: String){
        
        this._endereco = "Endereço incorreto ou não informado";
        this._bairro = "Bairro incorreto ou não informado";
        this._cep = "CEP incorreto ou não informado";
        this._cidade = "Cidade incorreta ou não informada";
        this._uf = "UF incorreta ou não informada";
        this._qtdQuartos = 0;
        this._qtdSalas = 0;
        this._qtdBanheiros = 0;
        this._qtdCozinhas = 0;
        this._andares = 0;
        this._complemento = "Comlemento incorreto ou não informado";
        this._valorVenal = 0;
        this._valorLocacao = 0;
        this._isLocavel = isLocavel;
        this._isVenal = isVenal;
        this._situacao = "Situação incorreta ou não informada";
        

      }
    
      public set endereco(endereco: String){
        if(endereco.trim().length > 1){
          this._endereco = endereco;
        }
      }
    
      public get endereco(): String{
        return this._endereco;
      }

      public set bairro(bairro: String){
        if(bairro.trim().length > 1){
          this._bairro = bairro;
        }
      }
    
      public get bairro(): String{
        return this._bairro;
      }

      public set cep(cep: String){
        if(cep.trim().length > 1){
          this._cep = cep;
        }
      }
    
      public get cep(): String{
        return this._cep;
      }

      public set cidade(cidade: String){
        if(cidade.trim().length > 1){
          this._cidade = cidade;
        }
      }
    
      public get cidade(): String{
        return this._cidade ;
      }
    
      public set uf(uf: String){
        if(uf.trim().length > 1){
          this._uf = uf;
        }
      }
    
      public get uf(): String{
        return this._uf ;
      }

      public set qtdQuartos(qtdQuartos: number){
          this._qtdQuartos = qtdQuartos;
      }
    
      public get qtdQuartos(): number{
        return this._qtdQuartos ;
      }

      public set qtdSalas(qtdSalas: number){
        this._qtdSalas = qtdSalas;
    }
  
    public get qtdSalas(): number{
      return this._qtdSalas;
    }

    public set qtdBanheiros(qtdBanheiros: number){
      this._qtdBanheiros = qtdBanheiros;
  }

  public get qtdBanheiros(): number{
    return this._qtdBanheiros;
  }
    //falta o get set da qtdCozinhas,andares, complemento, valorVenal, valorLocacao, isLocavel,i sVenal e situacao.
      public exibirDados(): String{
        return `Endereço: ${this._endereco} Bairro: ${this._bairro}
        CEP: ${this._cep} Cidade: ${this._cidade} 
        UF: ${this._uf} Quantidade de quartos: ${this._qtdQuartos}
        Quantidade de salas: ${this._qtdSalas} Quantidade de banheiros: ${this._qtdBanheiros}
        Quantidade de cozinhas: ${this._qtdCozinhas} Andares: ${this._andares}
        Complemento: ${this._complemento} Valor venal: ${this._valorVenal}
        Valor locação: ${this._valorLocacao} É Locavel: ${this._isLocavel} 
        É venal: ${this._isVenal} Situação: ${this._situacao}`;
      }
    }

