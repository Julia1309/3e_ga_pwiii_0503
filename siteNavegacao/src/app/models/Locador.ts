export class locador{
    _nome : String;
    _telefone: String; 
    _dataNascimento: Date;
    _email: String;
    _fone: String; 
    _sexo: String;
    _rendaMensal: Number;
    _nomeFiador: String;

    constructor(nome: String, telefone: String, dataNascimento: Date, email: String, fone: String, sexo: String, rendaMensal: Number, nomeFiador: String){
        
        this._nome = "Nome incorreto ou não informado";
        this._telefone = "Telefone incorreto ou não informado";
        this._dataNascimento = dataNascimento;
        this._email = "E-mail incorreto ou não informado";
        this._fone = "Fone incorreto ou não informado";
        this._sexo = "Sexo incorreto ou não informado";
        this._rendaMensal = 0;
        this._nomeFiador = "Nome do fiador incorreto ou não informado";
        

      }
    
      public set nome(nome: String){
        if(nome.trim().length > 1){
          this._nome = nome;
        }
      }
    
      public get nome(): String{
        return this._nome;
      }

      public set telefone(telefone: String){
        if(telefone.trim().length > 1){
          this._telefone = telefone;
        }
      }
    
      public get telefone(): String{
        return this._telefone;
      }

      public set dataNascimento(dataNascimento: Date){
          this._dataNascimento = dataNascimento;
      }
    
      public get dataNascimento(): Date{
        return this._dataNascimento;
      }

      public set email(email: String){
        if(email.trim().length > 1){
          this._email = email;
        }
      }
    
      public get email(): String{
        return this._email;
      }

      public set fone(fone: String){
        if(fone.trim().length > 1){
          this._fone = fone;
        }
      }
    
      public get fone(): String{
        return this._fone;
      }

      public set sexo(sexo: String){
        if(sexo.trim().length > 1){
          this._sexo = sexo;
        }
      }
    
      public get sexo(): String{
        return this._sexo;
      }

      public set rendaMensal(rendaMensal: Number){
          this._rendaMensal = rendaMensal;
      }
    
      public get rendaMensal(): Number{
        return this._rendaMensal;
      }

      public set nomeFiador(nomeFiador: String){
        if(nomeFiador.trim().length > 1){
          this._nomeFiador = nomeFiador;
        }
      }
    
      public get nomeFiador(): String{
        return this._nomeFiador;
      }
    
      public exibirDados(): String{
        return `Nome: ${this._nome} Telefone: ${this._telefone}
        Data de Nascimento: ${this._dataNascimento} Email: ${this._email}
        Fone: ${this._fone} Sexo: ${this._sexo} 
        Renda mensal: ${this._rendaMensal} Nome do fiador: ${this._nomeFiador}`;       
      }
}