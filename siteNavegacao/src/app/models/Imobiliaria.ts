export class imobiliaria{
    _nome: String;
    _endereco: String; //Rua e número
    _cidade: String;
    _bairro: String; 
    _fone: String;
    _site: String;

    constructor(){
        this._nome = "Nome incorreto ou não informado";
        this._endereco = "Endereço incorreto ou não informado";
        this._cidade = "Cidade incorreta ou não informada";
        this._bairro = "Bairro incorreto ou não informado";
        this._fone = "Fone incorreto ou não informado";
        this._site = "Site incorreto ou não informado";
      }
    
      public set nome(nome: String){
        if(nome.trim().length > 1){
          this._nome = nome;
        }
      }
    
      public get nome(): String{
        return this._nome;
      }

      public set endereco(endereco: String){
        if(endereco.trim().length > 1){
          this._endereco = endereco;
        }
      }
    
      public get endereco(): String{
        return this._endereco;
      }

      public set cidade(cidade: String){
        if(cidade.trim().length > 1){
          this._cidade = cidade;
        }
      }
    
      public get cidade(): String{
        return this._cidade;
      }

      public set bairro(bairro: String){
        if(bairro.trim().length > 1){
          this._bairro = bairro;
        }
      }
    
      public get bairro(): String{
        return this._bairro;
      }

      public set fone(fone: String){
        if(fone.trim().length > 1){
          this._fone = fone;
        }
      }
    
      public get fone(): String{
        return this._fone;
      }

      public set site(site: String){
        if(site.trim().length > 1){
          this._site = site;
        }
      }
    
      public get site(): String{
        return this._site;
      }
    
      public exibirDados(): String{
        return `Nome: ${this._nome} Endereço: ${this._endereco} 
        Cidade: ${this._cidade} Bairro: ${this._bairro}
        Fone: ${this._fone} Site: ${this._site}`;
      }
    }

