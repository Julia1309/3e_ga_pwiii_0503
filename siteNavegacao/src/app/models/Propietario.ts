export class propietario{
    _nome : String;
    _telefone: String; 
    _dataNascimento: Date;
    _sexo: String;
    _fone: String; 
    _email: String;

    constructor(nome: String, telefone: String, dataNascimento: Date, sexo: String, fone: String, email: String){
        
        this._nome = "Nome incorreto ou não informado";
        this._telefone = "Telefone incorreto ou não informado";
        this._dataNascimento = dataNascimento;
        this._sexo = "Sexo incorreto ou não informado";
        this._fone = "Fone incorreto ou não informado";
        this._email = "E-mail incorreto ou não informado";

      }
    
      public set nome(nome: String){
        if(nome.trim().length > 1){
          this._nome = nome;
        }
      }
    
      public get nome(): String{
        return this._nome;
      }

      public set telefone(telefone: String){
        if(telefone.trim().length > 1){
          this._telefone = telefone;
        }
      }
    
      public get telefone(): String{
        return this._telefone;
      }

      public set dataNascimento(dataNascimento: Date){
          this._dataNascimento = dataNascimento;
      }
    
      public get dataNascimento(): Date{
        return this._dataNascimento;
      }
    
      public set sexo(sexo: String){
        if(sexo.trim().length > 1){
          this._sexo = sexo;
        }
      }
    
      public get sexo(): String{
        return this._sexo;
      }

      public set fone(fone: String){
        if(fone.trim().length > 1){
          this._fone = fone;
        }
      }
    
      public get fone(): String{
        return this._fone;
      }

      public set email(email: String){
        if(email.trim().length > 1){
          this._email = email;
        }
      }
    
      public get email(): String{
        return this._email;
      }
      public exibirDados(): String{
        return `Nome: ${this._nome} Telefone: ${this._telefone}
        Data de nascimento: ${this._dataNascimento} Sexo: ${this._sexo} 
        Fone: ${this._fone} Email: ${this._email}`;
      }

}