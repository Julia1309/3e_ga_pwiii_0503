import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadPropietarioComponent } from './cad-propietario.component';

const routes: Routes = [
  {
    path: "",
    component: CadPropietarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadPropietarioRoutingModule { }
