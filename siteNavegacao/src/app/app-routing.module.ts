import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundModule } from './components/page-not-found/page-not-found.module';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home",
    loadChildren: () => import("./components/home/home.module").then(m => m.HomeModule)
  },
  {
    path: "cadimobiliaria",
    loadChildren: () => import("./components/cad-imobiliaria/cad-imobiliaria.module").then(m => m.CadImobiliariaModule)
  },
  {
    path: "cadimovel",
    loadChildren: () => import("./components/cad-imovel/cad-imovel.module").then(m => m.CadImovelModule)
  },
  {
    path: "cadlocador",
    loadChildren: () => import("./components/cad-locador/cad-locador.module").then(m => m.CadLocadorModule)
  },
  {
    path: "cadproprietario",
    loadChildren: () => import("./components/cad-propietario/cad-propietario.module").then(m => m.CadPropietarioModule)
  },
  {
    path: "**",
    loadChildren: () => import("./components/page-not-found/page-not-found.module")
    .then (m =>PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
